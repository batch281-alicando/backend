const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	
	user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true
	},
  product: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Product',
    required: true
  },
	orderDate: {
      type: Date,
      required: true
	},
    totalPrice: {
      type: Number,
      required: true
    },
    status: {
    type: String,
    required: true
    },
	isActive : {
		type: Boolean,
		default: true
	}

})

module.exports = mongoose.model("Order",orderSchema);