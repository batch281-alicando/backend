
// const Course = require("../models/Course");

// module.exports.addCourse = (reqBody) => {
//   // Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
//   // Uses the information from the request body to provide all the necessary information
// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	});

// 	return newCourse.save().then((course, error) => {
// 		if(error){
// 			return false;

// 		}else {
// 			return true;
// 		};
// 	})
// };


//add sir's code


const Course = require("../models/Course");

module.exports.addCourse = (data) =>{

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newCourse = new Course({
		name : data.course.name,
		description : data.course.description,
		price : data.course.price
	});

	return newCourse.save().then((course, error) => {
		if(error){
			return false;

		}else {
			return true;
		};
	})
};


module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	});
};

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	});
};

// Update a course
module.exports.updateCourse = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

		// Course not updated
		if(error){
			return false;

		// Course updated successfully
		} else {
			return true;
		}
	})

};


module.exports.archiveCourse = (reqParams, reqBody) => {
  // Specify the fields/properties of the document to be updated
  let archivedCourse = {
    isArchived: true
  };

  return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
    // Course not found
    if (!course) {
      return false;
    }
    // Course archived successfully
    return true;
  });
};