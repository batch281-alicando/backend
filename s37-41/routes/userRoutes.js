const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");
const Course = require("../routes/courseRoutes");
// Route for checking if the email already exists in the database
// Invokes the checkEmailExists function from the controller file
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// S38 ACTIVTY

// Route for retrieving user details
// The "auth.verify" acts as a middlware to ensure that the user is logged in before they can enroll to a course
router.get("/details", (req, res) => {

	// Uses decode method defined in the auth.js file to retrieve user information from the token passing the "token" from the request header
	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});


// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) =>{
    const data = {
        userId: auth.decode(req.headers.authorization).id,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        courseId: req.body.courseId
    }
    if(data.isAdmin == true){
        userController.enroll(data).then(resultFromController => res.send(resultFromController));
    }else {
        res.send(false)
    }

});

module.exports = router;
