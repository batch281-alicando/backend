const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require("../auth");
// Route for creating a course
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		courseController.addCourse(req.body).then((result) => {
			res.send(result);
	});
	
} else {
	res.send({ auth: "D ka admin teh"});
}
});
//retrieving all courses
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(
		resultFromController));
});
// router.post("/", (req, res) => {
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
// })


//retrieving all active courses
router.get("/", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(
		resultFromController));
	});

//retrieveing a specific course(by courseId)
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId)
	courseController.getCourse(req.params).then(resultFromController => res.send(
		resultFromController));
	});


// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

router.patch("/:courseId/archive", auth.verify, (req, res) => {
  courseController.archiveCourse(req.params, req.body).then(resultFromController => {
    if (resultFromController) {
      res.send("Course archived successfully.");
    } else {
      res.status(400).send("Failed to archive the course.");
    }
  });
});


// Allows us to export the "router" object that will be accessed in our index.js file
module.exports = router;







// //sir's code below


// const express = require("express");
// const router = express.Router();
// const courseController = require("../controllers/courseController");
// const auth = require("../auth");

// // Route for creating a course
// router.post("/", auth.verify, (req, res) => {
	
// 	const data = {
// 		course: req.body,
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}

// 	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

// });



// // Allows us to export the "router" object that will be accessed in our index.js file
// module.exports = router;
