const http = require('http');


const port = 3000;


const server = http.createServer((req, res) => {
  
  if (req.url === '/login') {
   
    console.log('User accessed the login page');
    res.end('You are on the login page');
  }
  else {
    
    console.log('Error: Invalid route');
    res.statusCode = 404;
    res.end('Error: Invalid route');
  }
});


server.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

