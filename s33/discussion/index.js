// console.log("Hello World");

// [Section] Javascript Synchronous vs Asynchronous
// JavaScript is by default synchronous meaning that only one statement is executed at a time.

// This can be proven when a statement has an error, javascript will not proceed with the next statement
console.log("Hello!");
// conosle.log("Hello Again!");
console.log("Goodbye");

// When certain statements takes a lot of time to process, this slows down our code
// An example of this are when loops are used on a large amount of information or when fetching data from databases
// When an action will take some time to process, this results in code "blocking"
// We might not notice it due to the fast processing power of our device
// This is the reason why some websites don't instantly load and we only see a white screen at times while the application is still waiting for all the code to be executed


// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

// Create a simple fetch request

// [Section] Getting all posts

// The fetch API allows you to asynchronously request for a resource(data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// Check the status of the request

// Retrieve all posts following the REST API(retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise
fetch('https://jsonplaceholder.typicode.com/posts')
// The "fetch" method will return a "promise" that resolves to a "Response" object
// The "then" method captures the "Response" object and returns another "promise" which will eventually be "Resolved" or "Rejected"
.then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())

.then((json) => console.log(json));

async function fetchData(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);
};

fetchData();

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/posts', {

	method: 'POST',
	headers: {
		'Content-Type' : 'application/jsonplaceholder',
	}, 
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello World!',
		userID: 1
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type' : 'application/jsonplaceholder',
	}, 
	body: JSON.stringify({
		title: 'Corrected Post',

	})
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json));
