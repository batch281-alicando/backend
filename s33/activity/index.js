//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]

async function getSingleToDo(id){
	try {
	    const response = await fetch('https://jsonplaceholder.typicode.com/todos/' + id);
	    const json = await response.json();
	    return json;
	  } catch (error) {
	    console.log('Error:', error);
	    return null;
	  }
	}

getSingleToDo(1)

  .then(json => console.log(json))
  .catch(error => console.log('Error:', error));


// Getting all to do list item
async function getAllToDo() {
  try {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos');
    const json = await response.json();
    return json.map(item => item.title);
  } catch (error) {
    console.log('Error:', error);
    return [];
  }
}

getAllToDo()
  .then(titles => console.log(titles))
  .catch(error => console.log('Error:', error));

// Creating to do list

async function createToDo(todoData) {
  try {
    const response = await fetch('https://jsonplaceholder.typicode.com/todos', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(todoData)
    });

    const json = await response.json();
    return json;
  } catch (error) {
    console.log('Error:', error);
    return null;
  }
}
const newTodo = {
  id: 201,
  title: 'Created To Do List Item',
  completed: false,
  userId: 1
};

createToDo(newTodo)
  .then(json => console.log(json))
  .catch(error => console.log('Error:', error));



// Updating to do list
async function updateToDo(id, updatedData) {
  try {
    const response = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(updatedData)
    });

    const json = await response.json();
    return json;
  } catch (error) {
    console.log('Error:', error);
    return null;
  }
}
const updatedData = {
  id: 1,
  Title: 'To Updat the my to do list with a different Title',
  Description: 'Updated Description',
  Status: 'Updated To Do List Item',
  'Date Completed': 'Pending',
  userId: 1
};

updateToDo(1, updatedData)
  .then(json => console.log(json))
  .catch(error => console.log('Error:', error));


// Deleting to do list
async function deleteToDo(id) {
  try {
    const response = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
      method: 'DELETE'
    });

    if (response.ok) {
      return 'Item deleted successfully';
    } else {
      throw new Error('Failed to delete item');
    }
  } catch (error) {
    console.log('Error:', error);
    return null;
  }
}

deleteToDo(1)
  .then(result => console.log(result))
  .catch(error => console.log('Error:', error));