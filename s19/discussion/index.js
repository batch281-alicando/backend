
  function checkHeight(height){
    if (height <= 150) {
      console.log("Did not pass the min height requirement.");
    }else{
      console.log("Passed the min height requirement.");
    }
}
let height = parseInt(prompt("Enter Height:"));
checkHeight(height);


// console.log("Hello World");


// [SECTION] Selection Control Structures
  /*
    - it sorts out whether the statement/s are to be executed based on the condition whether it is true or false.
    - two-way (true or false)
    - multi-way selection

  */

  // If ... else statement

  /*
    Syntax:
      if(condition) {
        //statement
      } else {
        //statement
      }
  
  */

  // If statement - executes a statement if a specified condition is true.

  let numA = -1;

  if(numA < 0) {
    console.log("Thu number is less than 0");
  }

  console.log(numA < 0);

  let city = "New York";

  if( city === "New York") {
    console.log("Welcome to New York City!");
  }


  // Else if
    /*
      - executes a statement if the previous condition/s are false and the if the specified condition is true.
      - the "else if" clause is optional and can be added to capture additionals conditions to change the flow of the programs.
    */

  let numB = 1;

  if(numA > 0) {
    console.log("Hello");
  } else if (numB > 0) {
    console.log("World");
  }


  city = "Tokyo"
  console.log(city)

  if(city === "New York") {
    console.log("Welcome to New York City!");
  } else if (city === "Tokyo") {
    console.log("Welcome to Tokyo!");
  }



  // Else
    //  - executes a statement if all of our previous conditions are false.


  if(numA > 0) {
    console.log("Hello");
  } else if(numB === 0) {
    console.log("World");
  } else {
    console.log("Again");
  }


  // let age = parseInt(prompt("Enter your age:"));

  // if(age <= 18) {
  //  console.log("Not allowed to drink");
  // } else {
  //  console.log("Matanda ka na! Shot puno!");
  // }



  /* 5 minutes
    Mini Activty
      -create a function that will receive any value of height as an argument when you invoke it.
      -then create conditional statements:
        - if height is less than or equal to 150, print "Did not pass the min height requirement." in the console.
        - but if the height is greater than 150, print "Passed the min height requirement." in the console
  */


function minHeight(height) {
  if(height <= 150) {
    console.log("Did not pass the min height requirement.");
  } else if(height > 150) {
    console.log("Passed the min height requirement.");
  }
}

// minHeight(parseInt(prompt("Enter your height")));
minHeight(160);
minHeight(149);



let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed) {
  if(windSpeed < 30) {
    return 'Not a typhoon'
  } else if(windSpeed <=61) {
    return 'Tropical depression detected'
  } else if(windSpeed >=62 && windSpeed <=88) {
    return 'Tropical storm detected'
  } else if(windSpeed >=89 && windSpeed <=177) {
    return 'Severe tropical storm detected'
  } else {
    return 'Typhoon detected'
  }
}

message = determineTyphoonIntensity(70);
// message = determineTyphoonIntensity(99);
// message = determineTyphoonIntensity(178);
console.log(message);
// console.log(determineTyphoonIntensity(99));


if(message == "Tropical storm detected") {
  console.warn(message);
}

// Truthy and Falsy

/*
  In JS a truthy value is a value that is considered true when encountered in a boolean context.

  Falsy value:
    1. false
    2. 0
    3. -0
    4. ""
    5. null
    6. undefined
    7. NaN
*/  

// Truthy Examples:

let word = "true"
if(word){
  console.log("Truthy");
};

if(true) {
  console.log("Truthy");
};

if(1) {
  console.log("Truthy");
};


// Falsy Examples:
if(false) {
  console.log("Falsy");
};

if(0) {
  console.log("Falsy");
};

if(undefined) {
  console.log("Falsy");
};

if(null) {
  console.log("Falsy");
};

if(-0) {
  console.log("Falsy");
};

if(NaN) {
  console.log("Falsy");
};



// Condition (Ternary) Operator - for short codes

/*
  Ternary operator takes in three operands
    1. condition
    2. expression to execute if the condition is true/truthy.
    3. expression to execute if the condition is false/falsy.

  Syntax:
    (condition) ? ifTrue_expression : isFalse_expression.

*/


// Single statement execution

let ternaryResult = (1 < 18) ? "Condition is True" : "Condition is False";

console.log("result of the ternary operator: " + ternaryResult);


// 


// Multiple statement execution

let name;

function isOfLegalAge() {
    name = "John";
    return "You are of the legal age limit";
}

function isUnderAge() {
    name = "Jane";
    return "You are under the age limit";
}

let yourAge = parseInt(prompt("what is your age?"));
console.log(yourAge);

let legalAge = (yourAge > 18) ? isOfLegalAge() : isUnderAge();
console.log(`result of ternary operator in function: ${legalAge}, ${name}`);


let day = prompt("What day of the week is it today?").toLowerCase();

consol.log(day);

switch (day){
case 'monday':
  console.log("The color of the day is yellow");
  break;
case 'tuesday':
  console.log("The color of the day is blue");
  break;
default:
  console.log("Please input day");
  break;
}


function  showIntensityAlert(windSpeed){

  try{
    alert(determinTyphoonIntensity(windSpeed))
  }
  catch(error){
    console.log(tyeoff error)
    console.log(error)
    console.log(error.message)
  }
  finally{
    alert("Intensity updates will show new alert!")
  }
}

showIntensityAlert(56);