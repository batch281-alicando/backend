
//task 1
db.fruits.count({ onSale: true });

//task 2

db.fruits.count({ stock: { $gt: 20 } });

//task 3

db.fruits.aggregate([
  { $match: { onSale: true } },
  {
    $group: {
      _id: "$supplier_id",
      averagePrice: { $avg: "$price" }
    }
  }
]);

//task 4

db.fruits.aggregate([
  {
    $group: {
      _id: "$supplier_id",
      maxPrice: { $max: "$price" }
    }
  }
]);

//task 5

db.fruits.aggregate([
  {
    $group: {
      _id: "$supplier_id",
      minPrice: { $min: "$price" }
    }
  }
]);