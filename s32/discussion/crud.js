let http = require("http");

let directory = [
	{
		"name" : "Brandon",
		"email" : "brandon@mail.com"
	},{
		"name" : "Brandon2",
		"email" : "brandon2@mail.com"
	}
];
let port = 4000;

let app = http.createServer(function(request, response){
	if(request.url == "/users" && request.method== "GET"){
		response.writeHead(200, {'Content-Type' : 'application/json'});
		response.write(JSON.stringify(directory));
		response.end();
	}
	if(request.url == "/users" && request.method== "POST"){
		let requestBody = '';

		request.on('data', function(data){
			requestBody += data;
		})
		request.on('end',function(){
			console.log(typeof requestBody);
			requestBody = JSON.parse(requestBody);
			let newUser = {
				"name" : requestBody.name,
				"email": requestBody.email
			}
			directory.push(newUser);
			console.log(directory);


			response.writeHead(200, {'Content-Type': 'application/json'})
			response.write(JSON.stringify(newUser));
			response.end();
		});
	}
	
});

app.listen(port, () => console.log('Server running at localhost:4000'));