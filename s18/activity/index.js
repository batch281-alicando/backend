/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/


function firstTask(){
	function checkSum(firstNum,secondNum){

	let sum = firstNum + secondNum;
	return sum;
}
	let firstNum = parseInt(prompt("Enter first number to add:")); 
	let secondNum = parseInt(prompt("Enter Second number to add:"));
	console.log("Displayed sum of "+firstNum+ " and "+secondNum)
	let sum = checkSum(firstNum, secondNum);
	console.log(sum);

}



function secondTask(){
	function checkDifference(firstNum,secondNum){

	let difference = firstNum - secondNum;
	return difference;
}


	let firstNum = parseInt(prompt("Enter first number to subtract:")); 
	let secondNum = parseInt(prompt("Enter Second number to subtract:"));
	console.log("Displayed difference of "+firstNum+ " and "+secondNum)
	let difference = checkDifference(firstNum, secondNum);
	console.log(difference);
}

function thirdTask(){
	function checkProduct(firstNum,secondNum){

	let product = firstNum * secondNum;
	return product;
}


	let firstNum = parseInt(prompt("Enter first number to multiply:")); 
	let secondNum = parseInt(prompt("Enter Second number to multiply:"));
	console.log("Displayed product of "+firstNum+ " and "+secondNum)
	let product = checkProduct(firstNum, secondNum);
	console.log(product);
}

function fourthTask(){
	function checkQuotient(firstNum,secondNum){

	let quotient = firstNum / secondNum;
	return quotient;
}


	let firstNum = parseInt(prompt("Enter first number  to divide:")); 
	let secondNum = parseInt(prompt("Enter Second number  to divide:"));
	console.log("Displayed quotient of "+firstNum+ " and "+secondNum)
	let quotient = checkQuotient(firstNum, secondNum);
	console.log(quotient);
}


	function getAreaOfCircle(){
		function getArea(radius){

		let area = radius*3.14;
		return area;
	}


		let radius = parseInt(prompt("Enter radius to get the area:")); 
		console.log("The result of getting the area of a circle with "+radius+" radius");
		let area = getArea(radius);
		console.log(area);
	}


	function findAverage(){
		function getAverage(firstNum, secondNum, thirdNum, fourthNum){
			let averageVar = (firstNum+secondNum+thirdNum+fourthNum) / 4;
			return averageVar;

	}
	let firstNum = parseInt(prompt("Enter first number  to average:")); 
	let secondNum = parseInt(prompt("Enter first number  to average:")); 
	let thirdNum = parseInt(prompt("Enter first number  to average:")); 
	let fourthNum = parseInt(prompt("Enter first number  to average:")); 
	console.log("The average of "+firstNum+ ", " +secondNum+ ", "+thirdNum+ ", "+fourthNum+":");
	let averageVar = getAverage(firstNum,secondNum,thirdNum,fourthNum);
	console.log(averageVar);
}


function checkGrade(){
function checkPassingScore(score, totalScore) {
  let percentage = (score / totalScore) * 100;
  let isPassed = percentage >= 75;
  return isPassed;
}
let score = parseInt(prompt("Enter score:")); 
let totalScore = parseInt(prompt("Enter number of items:"));
let isPassingScore = checkPassingScore(score, totalScore);
console.log("Is "+ score + "/" +totalScore + " a passing score?");

console.log(isPassingScore);

}





	firstTask();
	secondTask();
	thirdTask();
	fourthTask();
	getAreaOfCircle();
	findAverage();
	checkGrade();