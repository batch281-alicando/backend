// JSON Object
/*
	- JSON stands for JavaScript Object Notation
	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}

*/

// JSON as objects
/*{
	"city": "Quezon city",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// JSON Arrays
/*"cities": [
	{"city": "Quezon city", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila city", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makati city", "province": "Metro Manila", "country": "Philippines"}	
]*/

// Mini Activity - Create a JSON Array the will hold three breeds of dogs with properties: name, age, breed.

/*"dogs": [
	{"name": "Pochi", "age": "12", "breed": "boxer"},
	{"name": "Sasha", "age": "10", "breed": "poodle"},
	{"name": "Sachi", "age": "8", "breed": "dalmatian"}	
]*/

// JSON Methods

// Convert Data into Stringified JSON

let batchesArr = [{ batchName: 'Batch X' }, { batchName: 'Batch Y' }];

// the "stringify" method is used to convert JavaScript objects into a string
console.log('Result from stringify method:');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
});

console.log(data);

// Using Stringify method with variables
// User details
/*let firstName = prompt('What is your first name?');
let lastName = prompt('What is your last name?');
let age = prompt('What is your age?');
let address = {
	city: prompt('Which city do you live in?'),
	country: prompt('Which country does your city address belong to?')
};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData);*/

// Mini Activity - Create a JSON data that will accept user car details with variables brand, type, year.

/*let brand = prompt('Please enter car brand:');
let type = prompt('Please enter car type:');
let year = prompt('Please enter manufacture year:');


let carData = JSON.stringify({
	brand: brand,
	type: type,
	year: year
})

console.log(carData);*/

// Converting Stringified JSON into JavaScript Objects

let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

console.log('Result from parse method');
console.log(JSON.parse(batchesJSON));
