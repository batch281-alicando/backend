const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute")

const app = express();
const port = 4000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://francis-alicando:NJzKp4Nt7H05lttJ@cluster0.lljoaea.mongodb.net/",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true	
	}
);

app.use("/tasks", taskRoute)



if(require.main === module){
	app.listen(port, () => console.log(`Server running at ${port}`))
}

module.exports = app;