//task
db.rooms.insertOne({
  "name" : "single", 
  "accomonates" : 2,
  "price" : 1000,
  "description": "A simple room with all the basic necessities",
  "availableRooms" : 10,
  "isAvailable": "false"
});

//task
db.rooms.insertMany([
    {
      "name": "double",
      "accomodates": 3,
      "price": 2000,
      "description": "A room fit for a small family going on a vacation",
      "roomsAvailable": 5,
      "isAvailable": "false"
    },
    {
      "name": "queen",
      "accomodates": 4,
      "price": 4000,
      "description": "A room with a queen sized bed perfect for a simple getaway",
      "roomsAvailable": 15,
      "isAvailable": "false"
    }
  ]);

//task
db.rooms.find({"name": "double" });

//task

db.rooms.updateOne(
{ "name" : "queen"},
{
  $set : {
    "roomsAvailable": 0
  }
}
);

//task
db.rooms.deleteMany(
{ "roomsAvailable": 0},
{
  
}
);