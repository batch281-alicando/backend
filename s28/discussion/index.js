db.users.insertOne({
	"firstName" : "Jane", 
	"lastName" : "Doe",
	"age" : 21,
	"contact" : {
		"Phone" : "8123123",
		"email" : "janedoe@gmail.com"
	},
	"course" : ["css","JavaScript","Python"],
	"department" : "none"
});

db.users.insertMany([
	{
		"firstName" : "Jane", 
		"lastName" : "Doe",
		"age" : 21,
		"contact" : {
			"Phone" : "8123123",
			"email" : "janedoe@gmail.com"
		},
		"course" : ["css","JavaScript","Python"],
		"department" : "none"
	},
	{
		"firstName" : "Joghn", 
		"lastName" : "Doe",
		"age" : 11,
		"contact" : {
			"Phone" : "2123123",
			"email" : "johndoe@gmail.com"
		},
		"course" : ["css","JavaScript","Python"],
		"department" : "none"
	}
]);


db.users.find();

db.users.find({"firstName": "Jane" });

db.users.find({"_id" : ObjectId("64661745aa5530e5a41002ef")})
//INSERT ONE
db.users.insertOne({
	"firstName" : "Test", 
	"lastName" : "Test",
	"age" : 0,
	"contact" : {
		"Phone" : "00000000",
		"email" : "Test@gmail.com"
	},
	"course" : [],
	"department" : "none"
});
//UPDATE ONE
db.users.updateOne(
{ "firstName" : "Test"},
{
	$set : {
		"firstName" : "Bill", 
		"lastName" : "Gates",
		"age" : 66,
		"contact" : {
			"Phone" : "87654321",
			"email" : "bill@gmail.com"
		},
		"course" : ["PHP", "Laravel", "HTML"],
		"department" : "Operations",
		"Status" : "active"
	}
}
);
//UPDATE MANY USERS
db.users.updateMany(
{ "department" : "none"},
{
	$set : {
		"department" : "HR",
	}
}
);
//REPLACE 1
db.users.replaceOne(
{"firstName": "Bill"},
{
		"firstName" : "Bill", 
		"lastName" : "Gates",
		"age" : 66,
		"contact" : {
			"Phone" : "87654321",
			"email" : "bill@gmail.com"
		},
		"course" : ["PHP", "Laravel", "HTML"],
		"department" : "Operations",
}
);

db.users.insertOne({
	"firstName" : "Test"
});

db.users.deleteOne({
	"firstName" : "Test"
});